function location_field_load(map, location_based, zoom)
{
	return;
    var parent = map.parent().parent();

    var location_map;
    var changed_map = false;

    var location_coordinate = parent.find('input[type=text]');

    function savePosition(point)
    {
        location_coordinate.val(point.lat().toFixed(6) + "," + point.lng().toFixed(6));
        location_map.panTo(point);
        
    }

    function load() {
        var point = new google.maps.LatLng(1, 1);

        var options = {
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        
        location_map = new google.maps.Map(map[0], options);

        var initial_position;

        if (location_coordinate.val())
        {
            var l = location_coordinate.val().split(/,/);

            if (l.length > 1)
            {
                initial_position = new google.maps.LatLng(l[0], l[1]);
            }
        }

        var pinIcon = new google.maps.MarkerImage(
        	    "http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|FE7569",
        	    null, /* size is determined at runtime */
        	    null, /* origin is 0,0 */
        	    null, /* anchor is bottom center of the scaled image */
        	    new google.maps.Size(21, 34)
        	);  
        
        var marker = new google.maps.Marker({
            map: location_map,
            position: initial_position,
            draggable: true,
            icon: pinIcon
        });

        google.maps.event.addListener(marker, 'dragend', function(mouseEvent) {
        	
            savePosition(mouseEvent.latLng);
            location_based.each(function(i, f){
            	f.val("(position on map)");
            	f.blur();
            	changed_map = true;
            });
        });

        google.maps.event.addListener(location_map, 'click', function(mouseEvent){
        	
            marker.setPosition(mouseEvent.latLng);
            savePosition(mouseEvent.latLng);
            location_based.each(function(i, f){
            	f.val("(position on map)");
            	f.blur();
            	changed_map = true;
            });
        });

        var no_change = false;

        location_based.each(function(i, f)
        {
        	if(!f.length) return;
            var cb = function(){
                no_change = true;
                var lstr = [];
                location_based.each(function(a, b){
                    if (/select/i.test(b[0].tagName)) lstr.push(b.find('option:selected').html());
                    else lstr.push(b.val())
                })
                geocode(lstr.join(','), function(l){
                    location_coordinate.val(l.lat()+','+l.lng());
                    setTimeout(function(){ no_change = false; }, 2000);
                });
            };

            no_change = true;

            if (/select/i.test(f[0].tagName))
                f.change(cb);
            else f.keyup(cb);
            
            f.focus(function(){
            	if(changed_map){
            		changed_map = false;
            		f.val("");
            		
            	}
            });
        });

        location_coordinate.keyup(function(){
            if (no_change) return;
            var latlng = $(this).val().split(/,/);
            if (latlng.length < 2) return;
            var latlng = new google.maps.LatLng(latlng[0], latlng[1]);
            geocode_reverse(latlng, function(l){
                location_coordinate.val(l.lat()+','+l.lng());
            });
        });

        function placeMarker(location) {
            location_map.setZoom(zoom);
            marker.setPosition(location);
            location_map.setCenter(location);
            savePosition(location);
        }

        function geocode(address, cb) {
            var result;
            var geocoder = new google.maps.Geocoder();
            if (geocoder) {
                geocoder.geocode({"address": address}, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        cb(results[0].geometry.location);
                        placeMarker(results[0].geometry.location);
                    }
                });
            }
        }

        function geocode_reverse(location, cb) {
            var geocoder = new google.maps.Geocoder();
            if (geocoder) {
                geocoder.geocode({"latLng": location}, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        cb(results[0].geometry.location);
                        placeMarker(results[0].geometry.location);
                    }
                });
            }
        }

        placeMarker(initial_position);
    }

    load();

}

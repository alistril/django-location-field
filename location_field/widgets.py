from django.forms import widgets
from django.utils.safestring import mark_safe
from django.contrib.gis.geos import Point

class LocationWidget(widgets.TextInput):
    def __init__(self, attrs=None, based_fields=None, zoom=None, **kwargs):
        self.based_fields = based_fields
        self.zoom = zoom
        super(LocationWidget, self).__init__(attrs)

    def render(self, name, value, attrs=None):
        if value is not None:
            if isinstance(value, Point):
                value = '%s,%s' % (float(value[1]) , float(value[0]) )
        else:
            value = ''

        text_input = '<input id="id_location" name="%s" type="text" value="%s" />' % (name,value)         
        map_div = u'<div class="map-container-simple" id="map-container" style="width: 500px; height: 250px"><div id="map_location"></div></div>'
        related_fields = '<div class="location-block" data-fields="#id_%s" />' % self.based_fields[0].name;
        return mark_safe(related_fields + text_input + map_div)
